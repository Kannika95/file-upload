package com.example.demo.failfast;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FailFast {
	public static void main(String[] args)   
	{   
		Map<String, String> empName = new HashMap<String, String>();   
		empName.put("Sam Hanks", "New york");   
		empName.put("Will Smith", "LA");   
		empName.put("Scarlett", "Chicago");   
		Iterator<String> iterator = empName.keySet().iterator();   
		while (iterator.hasNext()) {   
			System.out.println(empName.get(iterator.next()));   
			// adding an element to Map   
			// exception will be thrown on next call   
			// of next() method.   
			empName.put("Scarlett", "Singai");   
			empName.put("Istanbul", "Turkey");   
		}   
	}

	private static void dosomething(Object obs) {
		System.out.println("object");
		
	}  
	
	private static void dosomething(String obs) {
		System.out.println("string");
		
	}  
}
