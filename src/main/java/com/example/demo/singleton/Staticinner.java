package com.example.demo.singleton;

public class Staticinner {
	private static class LoadSingleton {
	      static final Staticinner INSTANCE = new Staticinner();
	   }
	   private Staticinner() {}

	   public static Staticinner getInstance() {
		   System.out.println(EnumSingleton.GREEN.ordinal());
	      return LoadSingleton.INSTANCE;
	   }
}
