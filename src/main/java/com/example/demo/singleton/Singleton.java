package com.example.demo.singleton;

public class Singleton {

	private static Singleton instance = null;

	private Singleton() {
	}
	
	private Singleton(String a,int b) {
	}

	public static Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}
		return instance;
	}

}
