package com.example.demo.singleton;

public class Staticinitializer {
	private final static Staticinitializer instance; 
	   static { 
		instance = new Staticinitializer(); 
	   } 
	   private Staticinitializer() {} 
	   public static Staticinitializer getInstance() { 
		return instance; 
	   } 
}
