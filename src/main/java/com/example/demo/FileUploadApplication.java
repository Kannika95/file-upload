package com.example.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import com.example.demo.service.FileStorageService;

import jakarta.annotation.Resource;

@SpringBootApplication
@EnableDiscoveryClient
public class FileUploadApplication implements CommandLineRunner{

	@Resource
	FileStorageService storageService;

	public static void main(String[] args) throws Exception {
		SpringApplication.run(FileUploadApplication.class, args);
	}

	@Override
	public void run(String... arg) throws Exception {
//			    storageService.deleteAll();
		storageService.init();
	}

}
